import sys

input_filename = sys.argv[1]
aux_file_name = 'auxiliary_file'
aux_file = open(aux_file_name, "w")
input_file = open(input_filename, "r")

# reading lines from logfile and saving all urls to the auxiliary file:
for line in input_file:
    info = line.split('"')
    response = info[1]
    start_idx = response.find(' ')
    response = response[start_idx+1:]
    stop_idx = response.find(' ')
    url = response[:stop_idx]
    qm_idx = url.rfind('?')
    if qm_idx != -1:
        url = url[:qm_idx]
    if url[len(url)-1] == '/':
        url = url[:-1]
    double_slash_idx = url.find('//')
    stripped_url = url[double_slash_idx+2:]
    aux_file.write(stripped_url + '\n')
aux_file.close()

# getting set of urls from auxiliary file:
aux_file = open(aux_file_name, "r")
url_set = set(aux_file.readlines())
aux_file.close()

# counting number of requests:
records = []
for each_url in url_set:
    counter = 0
    aux_file = open(aux_file_name, "r")
    for each_request in aux_file:
        if each_request == each_url:
            counter += 1
    pair = (each_url.strip(),counter)
    records.append(pair)
records.sort(key=lambda x: x[1], reverse=True)

# sorting records in lexicographical order:
for i in range(1,len(records)):
    if records[i][1] == records[i-1][1]:
        temp_list = []
        temp_list.append(records[i-1])
        temp_list.append(records[i])
        temp_list.sort()
        records[i-1] = temp_list[0]
        records[i] = temp_list[1]

# printing records line by line (to csv file):
for rec in records:
    print('"' + rec[0] + '",' + str(rec[1]))
input_file.close()
aux_file.close()