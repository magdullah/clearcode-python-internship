# =============
# hack_power.py 
# =============
# File contains script with two functions: 
# - letter_power(hack,letter) - returns power of chosen letter in given hack
# - hack_calculator(hack) - returns power of given hack.
# In a while loop in hack_calculator(hack)function, we check if there is "ba" string. If this "ba" is part of "baa", we increase power with 20, if it's not - we increase power with 10. After that, we cut out "ba" or "baa" from the hack and continue checking.
#
# ==============
# page_report.py
# ==============
# File contains script prepared to be opened in command line with following prompt: python page_report.py today.log > report.csv
# It uses auxiliary file to store some data.
# 

