def letter_power(hack, letter):
    idx = hack.count(letter)
    power_sum = 0
    if letter == 'a':
        power = 1
    elif letter == 'b':
        power = 2
    elif letter == 'c':
        power = 3
    else:
        power = 0
    for i in range(idx):
        power_sum += power * (i+1)
    return power_sum

def hack_calculator(hack):
    validation = True
    for letter in hack:
        if (letter != 'a') and (letter != 'b') and (letter != 'c'):
            validation = False
    if validation:
        a_sum = letter_power(hack, 'a')
        b_sum = letter_power(hack, 'b')
        c_sum = letter_power(hack, 'c')
        ba_sum = 0
        baa_sum = 0
        phrase_idx = hack.find('ba')
        while phrase_idx != -1:
            hack = hack[phrase_idx:]
            if len(hack) >= 3:
                if hack[phrase_idx + 2] == 'a':
                    baa_sum += 20
                else:
                    ba_sum += 10
            else:
                ba_sum += 10
            hack = hack[phrase_idx + 2:]
            phrase_idx = hack.find('ba')
        all_sum = a_sum + b_sum + c_sum + baa_sum + ba_sum
    else:
        all_sum = 0
    return all_sum

hackword = input("Write your hack: ")
total_power = hack_calculator(hackword)
print(total_power)
